package edu.javacourse.studentorder.dao;

import edu.javacourse.studentorder.domain.*;
import edu.javacourse.studentorder.exception.DaoException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class StudentOrderDaoImplTest {

    @BeforeClass
    public static void startUp() throws Exception {
        DBInit.startUp ();

    }

    @Test
    public void saveStudentOrder() throws DaoException {
        StudentOrder so = buildStudentOrder ( 10 );
        Long id = new StudentOrderDaoImpl ().saveStudentOrder ( so );
    }

    @Test(expected = DaoException.class)
    public void saveStudentOrderError() throws DaoException {
            StudentOrder so = buildStudentOrder ( 10 );
            so.getHusband ().setSurName ( null );
            Long id = new StudentOrderDaoImpl ().saveStudentOrder ( so );
    }

    @Test
    public void getStudentOrders() throws DaoException {
        List<StudentOrder> list = new StudentOrderDaoImpl ().getStudentOrders ();
    }


    public static StudentOrder buildStudentOrder(long id) {
        StudentOrder so = new StudentOrder();
        so.setStudentOrderId(id);
        so.setMarriageCertificateId("" + (123456000 + id));
        so.setMarriageDate(LocalDate.of(2016, 7, 4));

        RegisterOffice ro = new RegisterOffice(1L, "", "");
        so.setMarriageOffice(ro);

        Street street = new Street(1L, "First Street");

        Address address = new Address("195000", street, "12", "", "142");

        //Муж
        Adult husband = new Adult("Петров", "Виктор", "Сергеевич", LocalDate.of(1998, 3, 12));
        husband.setPassportSerial("" + (1000 + id));
        husband.setPassportNumber("" + (100000 + id));
        husband.setIssueDate(LocalDate.of(2017, 9, 15));
        PassportOffice po1 = new PassportOffice(1L, "", "");
        husband.setIssueDepartment(po1);
        husband.setStudentId("" + (100000) + id);
        husband.setAddress(address);
        husband.setUniversity ( new University (2L,"") );
        husband.setStudentId ( "HH12345" );
//        System.out.println("INFO" + LocalDateTime.now() + "     buildStudentOrder     " + husband);

        //Жена
        Adult wife = new Adult("Петрова", "Вероника", "Алексеевна", LocalDate.of(1998, 3, 12));
        wife.setPassportSerial("" + (2000 + id));
        wife.setPassportNumber("" + (200000 + id));
        wife.setIssueDate(LocalDate.of(2018, 4, 5));
        PassportOffice po2 = new PassportOffice(2L, "", "");
        wife.setIssueDepartment(po2);
        wife.setStudentId("" + (200000) + id);
        wife.setAddress(address);
        wife.setUniversity ( new University (1L,"") );
        wife.setStudentId ( "WW12345" );
//        System.out.println("INFO" + LocalDateTime.now() + "     buildStudentOrder     " + wife);

        //Ребенок
        Child child1 = new Child("Петрова", "Ирина", "Викторовна", LocalDate.of(2018, 6, 29));
        child1.setCertificateNumber("" + (300000 + id));
        child1.setIssueDate((LocalDate.of(2018, 7, 19)));
        RegisterOffice ro2 = new RegisterOffice(2L, "", "");
        child1.setIssueDepartment(ro2);
        child1.setAddress(address);
//        System.out.println("INFO" + LocalDateTime.now() + "     buildStudentOrder     " + child1);

        Child child2 = new Child("Петров", "Илья", "Викторович", LocalDate.of(2018, 6, 29));
        child2.setCertificateNumber("" + (400000 + id));
        child2.setIssueDate((LocalDate.of(2018, 7, 19)));
        RegisterOffice ro3 = new RegisterOffice(2L, "", "");
        child2.setIssueDepartment(ro3);
        child2.setAddress(address);
//        System.out.println("INFO" + LocalDateTime.now() + "     buildStudentOrder     " + child2);


        so.setHusband(husband);
        so.setWife(wife);
        so.addChild(child1);
        so.addChild(child2);

//        System.out.println("ERROR" + LocalDateTime.now() + "     buildStudentOrder     " + so);
        return so;

    }
}