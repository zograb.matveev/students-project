package edu.javacourse.studentorder.domain;

public class Street {
    private Long StreetCode;
    private String StreetName;

    public Street() {
    }

    public Street(Long streetCode, String streetName) {
        StreetCode = streetCode;
        StreetName = streetName;
    }

    public Long getStreetCode() {
        return StreetCode;
    }

    public void setStreetCode(Long streetCode) {
        StreetCode = streetCode;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String streetName) {
        StreetName = streetName;
    }
}
